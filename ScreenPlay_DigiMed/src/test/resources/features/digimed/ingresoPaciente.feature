# language: es
# author: Esteban

Característica: Ingreso Paciente
  Como médico
  quiero ingresar la información de mi paciente
  para poder atenderlo

  @IngresarAlPaciente
  Esquema del escenario: Ingreso nuevo paciente
    Dado que el medico se encuentra logueado en el aplicativo digimed
      | user                   | password |
      | digimedsofka@gmail.com | hola123+ |
    Cuando se dirige al apartado nuevo paciente e ingresa el documento del nuevo paciente con sus datos
    Y diligencia el formulario de sintomas del paciente
    Y se le asigna un diagnostico
    Y ingresa el tratamiento y selecciona la <opcion> para remitir al paciente
    Y verifica la bandeja de entrada del email "personal1" de la "enfermera" asignada
    Entonces  debera mostrarle un mensaje de "notificacion de funcion para paciente"

    Ejemplos:
      | opcion     |
      | "Ingresar" |

  @RemitirAlPaciente
  Esquema del escenario: Remitir nuevo paciente
    Dado que el medico se encuentra logueado en el aplicativo digimed
      | user                   | password |
      | digimedsofka@gmail.com | hola123+ |
    Cuando se dirige al apartado nuevo paciente e ingresa el documento del nuevo paciente con sus datos
    Y diligencia el formulario de sintomas del paciente
    Y se le asigna un diagnostico
    Y ingresa el tratamiento y selecciona la <opcion> para remitir al paciente
    Entonces el sistema debera mostrarle un mensaje de atencion finalizada

    Ejemplos:
      | opcion                       |
      | "Remitir a otra institución" |


  @DarDeAltaAlPaciente
  Esquema del escenario: Remitir paciente
    Dado que el medico se encuentra logueado en el aplicativo digimed
      | user                   | password |
      | digimedsofka@gmail.com | hola123+ |
    Cuando se dirige al apartado nuevo paciente e ingresa el documento del paciente ya existente
    Y diligencia el formulario de sintomas del paciente
    Y se le asigna un diagnostico
    Y ingresa el tratamiento y selecciona la <opcion> para remitir al paciente
    Y verifica la cita asignada en la bandeja de entrada del email del "paciente" "drprofesorjosevicentevelasco"
    Entonces debera ver el correo con el mensaje "Cita programada"

    Ejemplos:
      | opcion                       |
      | "Dar de alta" |


