package co.com.sofka.stepdefinitions.setup;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.thucydides.core.annotations.Managed;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static co.com.sofka.utils.Dictionary.ACTOR_NAME;
import static co.com.sofka.utils.digimed.enums.Log4jValues.LOG4J_PROPERTIES_FILE_PATH_WINDOWS;
import static co.com.sofka.utils.digimed.enums.Log4jValues.USER_DIRECTORY;
import static co.com.sofka.utils.switches.chrome.ChromeSwitches.*;

public class SetUp {
    @Managed
    protected WebDriver webDriver;

    private void setUpBrowser() {
        WebDriverManager.chromedriver().win().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments(INCOGNITO, START_MAXIMIZED, DISABLE_NOTIFICATIONS, DISABLE_PLUGINS);
        webDriver = new ChromeDriver(options);
    }

    private void setUpStageAndActor() {
        OnStage.setTheStage(Cast.ofStandardActors());
        OnStage.theActorCalled(ACTOR_NAME);
        OnStage.theActorInTheSpotlight().can(BrowseTheWeb.with(webDriver));
    }

    private void configureLog4J() {
        PropertyConfigurator.configure(USER_DIRECTORY.getValue() + LOG4J_PROPERTIES_FILE_PATH_WINDOWS.getValue());
    }

    protected void generalSetUp() {
        setUpBrowser();
        setUpStageAndActor();
        configureLog4J();
    }
}
