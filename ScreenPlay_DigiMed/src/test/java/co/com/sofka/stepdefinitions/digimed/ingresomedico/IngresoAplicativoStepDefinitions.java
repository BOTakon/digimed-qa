package co.com.sofka.stepdefinitions.digimed.ingresomedico;

import co.com.sofka.stepdefinitions.setup.SetUp;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.questions.digimed.InvalidUserMessage.invalidUserMessage;
import static co.com.sofka.questions.digimed.VisibleMainPage.visibleMainPage;
import static co.com.sofka.tasks.digimed.LogIn.logInWith;
import static co.com.sofka.tasks.digimed.OpenLoginPage.openLoginPage;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.*;

public class IngresoAplicativoStepDefinitions extends SetUp {
    @Before
    public void setUp() {
        generalSetUp();
    }

    @Dado("el medico se encuentra en el aplicativo Digimed")
    public void elMedicoSeEncuentraEnElAplicativoDigimed() {
        theActorInTheSpotlight().wasAbleTo(openLoginPage());
    }

    @Cuando("se dirige al apartado de ingreso y se intenta logear con las credenciales")
    public void seDirigeAlApartadoDeIngresoYSeIntentaLogearConLasCredenciales(DataTable credentials) {
        theActorInTheSpotlight().attemptsTo(logInWith(credentials));
    }

    @Entonces("deberia de ver la main page del aplicativo")
    public void deberiaDeVerLaMainPageDelAplicativo() {
        theActorInTheSpotlight().should(seeThat(visibleMainPage(), is(true)));
    }

    //@IngresoFallido
    @Cuando("se dirige al apartado de ingreso y se intenta logear con credenciales incorrectos")
    public void seDirigeAlApartadoDeIngresoYSeIntentaLogearConCredencialesIncorrectos(DataTable credentials) {
        theActorInTheSpotlight().attemptsTo(logInWith(credentials));
    }

    @Entonces("el sistema deberia mostrarle el mensaje de error {string}")
    public void elSistemaDeberiaMostrarleElMensajeDeError(String expectedMessage) {
        theActorInTheSpotlight().should(seeThat(invalidUserMessage(), equalTo(expectedMessage)));
    }
}
