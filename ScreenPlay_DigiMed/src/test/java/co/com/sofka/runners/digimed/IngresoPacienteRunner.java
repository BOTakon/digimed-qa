package co.com.sofka.runners.digimed;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/digimed/ingresoPaciente.feature",
        glue = "co.com.sofka.stepdefinitions.digimed.ingresopaciente",
        tags = "",
        snippets = CucumberOptions.SnippetType.CAMELCASE)
public class IngresoPacienteRunner {
}
