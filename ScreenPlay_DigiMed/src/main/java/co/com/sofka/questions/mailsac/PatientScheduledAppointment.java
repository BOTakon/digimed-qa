package co.com.sofka.questions.mailsac;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.sofka.userinterfaces.mailsac.InboxPage.SCHEDULED_APPOINTMENT_NOTIFICATION;


public class PatientScheduledAppointment implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(SCHEDULED_APPOINTMENT_NOTIFICATION).answeredBy(actor);
    }

    public static PatientScheduledAppointment patientScheduledAppointment() {
        return new PatientScheduledAppointment();
    }
}
