package co.com.sofka.questions.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Visibility;

import static co.com.sofka.userinterfaces.digimed.MainPage.MAIN_PAGE;

public class VisibleMainPage implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        return Visibility.of(MAIN_PAGE).answeredBy(actor);
    }

    public static VisibleMainPage visibleMainPage() {
        return new VisibleMainPage();
    }
}
