package co.com.sofka.questions.mailsac;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.sofka.userinterfaces.mailsac.InboxPage.MESSAGE_NOTIFICATION_NURSE;

public class PatientNotificationFunction implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(MESSAGE_NOTIFICATION_NURSE).answeredBy(actor);
    }

    public static PatientNotificationFunction patientNotificationFunction() {
        return new PatientNotificationFunction();
    }
}
