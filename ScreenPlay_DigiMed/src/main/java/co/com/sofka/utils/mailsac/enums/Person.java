package co.com.sofka.utils.mailsac.enums;

public enum Person {
    NURSE("enfermera"),
    PATIENT("paciente");

    private final String value;

    Person(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
