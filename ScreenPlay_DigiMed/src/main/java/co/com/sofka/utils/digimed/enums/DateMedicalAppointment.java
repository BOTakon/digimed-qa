package co.com.sofka.utils.digimed.enums;

public enum DateMedicalAppointment {
    DATE_MEDICAL_APPOINTMENT("12082022"),
    DATE_HOUR("0530");

    private final String value;

    DateMedicalAppointment(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
