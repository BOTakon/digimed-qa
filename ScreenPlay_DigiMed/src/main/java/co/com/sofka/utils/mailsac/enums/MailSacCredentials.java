package co.com.sofka.utils.mailsac.enums;

public enum MailSacCredentials {
    MAIL_USERNAME("DigimedPrueba"),
    MAIL_PASSWORD("123456789u");

    private final String value;

    MailSacCredentials(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
