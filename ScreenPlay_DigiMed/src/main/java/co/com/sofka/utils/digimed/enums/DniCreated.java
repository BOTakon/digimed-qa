package co.com.sofka.utils.digimed.enums;

public enum DniCreated {
    DNI_CREATED("9998887766");

    private final String value;

    DniCreated(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
