package co.com.sofka.utils.digimed.faker;

import co.com.sofka.models.digimed.PatientInformation;
import com.github.javafaker.Faker;

public class FakerPatientInformation {
    private static Faker faker = new Faker();

    private FakerPatientInformation() {

    }

    public static PatientInformation randomPatient() {
        PatientInformation patientInformation = new PatientInformation();
        patientInformation.setDni(faker.number().digits(10));
        patientInformation.setName(faker.name().firstName());
        patientInformation.setEps(faker.company().name());
        patientInformation.setEmail(faker.internet().emailAddress());
        patientInformation.setCelular(faker.number().digits(10));
        return patientInformation;
    }

    public static String fakerSymptomsForm() {
        String form;
        form = faker.medical().symptoms();
        return form;
    }

    public static String fakerDiagnosis() {
        String diagnosis;
        diagnosis = faker.medical().diseaseName();
        return diagnosis;
    }

    public static String fakerTreatment(){        
        return faker.medical().medicineName();
    }
}

