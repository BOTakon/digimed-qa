package co.com.sofka.utils.switches.chrome;

public class ChromeSwitches {
    public static final String INCOGNITO = "--incognito";
    public static final String START_MAXIMIZED = "--Start-maximized";
    public static final String DISABLE_NOTIFICATIONS = "--disable-notifications";
    public static final String DISABLE_PLUGINS  = "--disable-plugins";
}
