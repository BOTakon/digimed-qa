package co.com.sofka.userinterfaces.mailsac;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class MailsacMainPage extends PageObject {
    public static final Target MALSAC_PAGE =
            Target.the("Mailsac page")
                    .located(By.xpath("/html[@ng-app='mailsac']"));

    public static final Target MAILSAC_MENU_LIST =
            Target.the("Mailsac menu list")
                    .located(By.xpath("//a[@class='dropdown-toggle']"));

    public static final Target SIGN_IN =
            Target.the("Sign in")
                    .located(By.xpath("//a[contains(string(), 'Sign In')]"));

    public static final Target MAILSAC_EMAIL_FIELD =
            Target.the("Mailsac email field")
                    .located(By.xpath("/html/body/div/div[3]/div[1]/div/div[1]/div/div/div/input[1]"));

    public static final Target CHECK_THE_MAIL =
            Target.the("Check the mail")
                    .located(By.xpath("//button[@class='btn btn-primary btn-block']"));
}
