package co.com.sofka.userinterfaces.mailsac;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SignInPage extends PageObject {
    public static final Target MAILSAC_USERNAME =
            Target.the("Mailsac username")
                    .located(By.xpath("//input[@name='username']"));

    public static final Target MAILSAC_PASSWORD =
            Target.the("Mailsac password")
                    .located(By.xpath("//input[@name='password']"));

    public static final Target SIGN_IN_BUTTON =
            Target.the("Sign in button")
                    .located(By.xpath("//button[@type='submit']"));
}
