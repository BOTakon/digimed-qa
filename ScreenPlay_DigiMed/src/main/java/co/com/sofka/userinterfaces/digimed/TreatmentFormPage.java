package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class TreatmentFormPage extends PageObject {
    public static final Target TREATMENT_FORM_FIELD =
            Target.the("Treatment text field")
                    .located(By.xpath("//textarea[@id='procedimiento-text']"));

    public static final Target RADIO_INPUT_CHECKIN =
            Target.the("Opción ingresar")
                    .located(By.id("estado-ingresado"));

    public static final Target RADIO_INPUT_RELEASE =
            Target.the("Opción dar de alta")
                    .located(By.id("estado-alta"));

    public static final Target RADIO_INPUT_TRANSFER =
            Target.the("Opción remitir a otra entidad")
                    .located(By.id("estado-remitido"));

    public static final Target BUTTON_FINISH_ATTENTION =
            Target.the("Botón Fin atención")
                    .located(By.id("enviar-tratamiento"));

    public static final Target ADD_MEDICAL_APPOINTMENT =
            Target.the("Add meedical appointment")
                    .located(By.xpath("//input[@id='agregar-cita']"));

    public static final Target MEDICAL_APPOINTMENT_DATE =
            Target.the("Medical appointment date")
                    .located(By.xpath("//input[@id='fecha-cita']"));

}
