package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class MainPage extends PageObject {
    public static final Target MAIN_PAGE =
            Target.the("Main page")
                    .located(By.xpath("//div[@class='bg-white vh-100']"));

    public static final Target NEW_PATIENT =
            Target.the("Nuevo paciente")
                    .located(By.xpath("//h3[@class='titulo-h3-card-option '][contains(string(), 'Nuevo paciente')]"));

    //for assertions
    public static final Target SUCCESSFUL_ATTENTION_ALERT =
            Target.the("Alerta de atención finalizada")
                    .located(By.cssSelector(".swal2-popup"));
}
