package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class DiagnosisPage extends PageObject {
    public static final Target DIAGNOSIS_FORM =
            Target.the("Diagnosis form")
                    .located(By.id("exampleFormControlTextarea1"));

    public static final Target SEND_DIAGNOSIS =
            Target.the("Send diagnosis")
                    .located(By.xpath("//button[@type='submit'][contains(string(),'Enviar')]"));
}
