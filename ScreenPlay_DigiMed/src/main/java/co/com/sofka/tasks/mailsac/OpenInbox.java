package co.com.sofka.tasks.mailsac;

import co.com.sofka.userinterfaces.mailsac.InboxPage;
import co.com.sofka.utils.mailsac.enums.Person;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.conditions.Check;

import static co.com.sofka.userinterfaces.mailsac.InboxPage.*;
import static co.com.sofka.userinterfaces.mailsac.MailsacMainPage.*;
import static co.com.sofka.userinterfaces.mailsac.SignInPage.*;
import static co.com.sofka.utils.Dictionary.EMAIL_URL;
import static co.com.sofka.utils.mailsac.enums.MailSacCredentials.*;
import static co.com.sofka.utils.mailsac.enums.Person.*;
import static org.openqa.selenium.WindowType.WINDOW;

public class OpenInbox implements Task {
    private String email;
    private String person;

    public OpenInbox with(String email) {
        this.email = email;
        return this;
    }

    public OpenInbox ofThe(String person) {
        this.person = person;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Switch.toNewWindow(WINDOW),
                Open.url(EMAIL_URL),
                Click.on(MALSAC_PAGE),
                Click.on(MAILSAC_MENU_LIST),
                Click.on(SIGN_IN),
                SendKeys.of(MAIL_USERNAME.getValue()).into(MAILSAC_USERNAME),
                SendKeys.of(MAIL_PASSWORD.getValue()).into(MAILSAC_PASSWORD),
                Click.on(SIGN_IN_BUTTON),
                Click.on(MAILSAC_EMAIL_FIELD),
                SendKeys.of(email).into(MAILSAC_EMAIL_FIELD),
                Click.on(CHECK_THE_MAIL),
                Check.whether(person.equals(NURSE.getValue()))
                        .andIfSo(Click.on(PATIENT_TREATMEN_MAIL))
                        .otherwise(Check.whether(person.equals(PATIENT.getValue()))
                                .andIfSo(Click.on(PATIENT_SCHEDULED_APPOINTMENT)))
        );
    }

    public static OpenInbox openInbox() {
        return new OpenInbox();
    }
}

