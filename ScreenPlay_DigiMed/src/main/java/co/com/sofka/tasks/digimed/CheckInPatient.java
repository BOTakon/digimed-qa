package co.com.sofka.tasks.digimed;

import co.com.sofka.models.digimed.PatientInformation;
import co.com.sofka.userinterfaces.digimed.MainPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static co.com.sofka.userinterfaces.digimed.MainPage.*;
import static co.com.sofka.userinterfaces.digimed.NewPatientPage.*;
import static co.com.sofka.utils.digimed.faker.FakerPatientInformation.randomPatient;

public class CheckInPatient implements Task {
    private PatientInformation patientInformation;

    @Override
    public <T extends Actor> void performAs(T actor) {
        patientInformation = randomPatient();
        actor.attemptsTo(
                Click.on(NEW_PATIENT),
                SendKeys.of(patientInformation.getDni()).into(VERIFY_DNI),
                Click.on(VERIFY_BUTTON),
                Click.on(NAME_FIELD),
                SendKeys.of(patientInformation.getName()).into(NAME_FIELD),
                SendKeys.of(patientInformation.getDni()).into(DNI),
                SendKeys.of(patientInformation.getEps()).into(EPS),
                SendKeys.of(patientInformation.getEmail()).into(EMAIL),
                SendKeys.of(patientInformation.getCelular()).into(CELLPHONE),
                Click.on(SEND_BUTTON)
        );
    }

    public static CheckInPatient checkInPatient() {
        return new CheckInPatient();
    }
}
