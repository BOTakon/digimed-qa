package co.com.sofka.tasks.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static co.com.sofka.utils.Dictionary.DIGIMED_URL;

public class OpenLoginPage implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Open.url(DIGIMED_URL)
        );
    }

    public static OpenLoginPage openLoginPage() {
        return new OpenLoginPage();
    }
}
