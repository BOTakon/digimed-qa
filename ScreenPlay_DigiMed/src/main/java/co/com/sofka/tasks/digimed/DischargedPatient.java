package co.com.sofka.tasks.digimed;

import co.com.sofka.interaction.digimed.SendDateMedicalAppointment;
import co.com.sofka.userinterfaces.digimed.TreatmentFormPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class DischargedPatient implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TreatmentFormPage.RADIO_INPUT_RELEASE),
                Click.on(TreatmentFormPage.ADD_MEDICAL_APPOINTMENT),
                Click.on(TreatmentFormPage.MEDICAL_APPOINTMENT_DATE),
                SendDateMedicalAppointment.on(TreatmentFormPage.MEDICAL_APPOINTMENT_DATE),
                Click.on(TreatmentFormPage.BUTTON_FINISH_ATTENTION)
        );
    }

    public static DischargedPatient dischargedPatient() {
        return new DischargedPatient();
    }
}
