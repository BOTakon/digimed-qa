package co.com.sofka.tasks.digimed;

import co.com.sofka.models.digimed.LoginCredentials;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static co.com.sofka.userinterfaces.digimed.LoginPage.*;
import static co.com.sofka.utils.digimed.converdatatables.ConvertLoginCredentials.convertDataTableToLoginInfo;

public class LogIn implements Task {
    private LoginCredentials loginCredentials;
    private DataTable dataTable;

    public LogIn(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        loginCredentials = convertDataTableToLoginInfo(dataTable);
        actor.attemptsTo(
                Click.on(USER_EMAIL),
                SendKeys.of(loginCredentials.getUser()).into(USER_EMAIL),
                SendKeys.of(loginCredentials.getPassword()).into(USER_PASSWORD),
                Click.on(SUBMIT_BUTTON)
        );
    }

    public static LogIn logInWith(DataTable credentials) {
        return new LogIn(credentials);
    }
}
