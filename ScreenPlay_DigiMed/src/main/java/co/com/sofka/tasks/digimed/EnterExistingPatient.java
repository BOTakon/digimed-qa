package co.com.sofka.tasks.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static co.com.sofka.userinterfaces.digimed.MainPage.NEW_PATIENT;
import static co.com.sofka.userinterfaces.digimed.NewPatientPage.VERIFY_BUTTON;
import static co.com.sofka.userinterfaces.digimed.NewPatientPage.VERIFY_DNI;
import static co.com.sofka.utils.digimed.enums.DniCreated.*;

public class EnterExistingPatient implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(NEW_PATIENT),
                SendKeys.of(DNI_CREATED.getValue()).into(VERIFY_DNI),
                Click.on(VERIFY_BUTTON)
        );
    }

    public static EnterExistingPatient enterExistingPatient() {
        return new EnterExistingPatient();
    }
}
